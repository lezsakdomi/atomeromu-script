const readline = require('readline')
const http = require('http')
const queryString = require('querystring')
const windows1250 = require('windows-1250')
const striptags = require('striptags')
const fs = require('fs')
const commander = require('commander')

const argv = commander
	.option('-i, --input <file>', "Input file, console if omitted")
	.option('-o, --output <file>', "Output file, console if omitted")
	.option('--interactive', "Force interactive mode even if input and output files provided")
	.option('--noninteractive', "Run in non-interactive mode even on stdin/stdout")
	.parse(process.argv)

const rl = readline.createInterface({
	input: argv.input ? fs.createReadStream(argv.input) : process.stdin,
	output: argv.output ? fs.createWriteStream(argv.output) : process.stdout,
})

let phpsessid = null

let state = {}

const interactive = argv.interactive || !(argv.input || argv.output || argv.noninteractive)

main(rl)

async function main(rl) {
	if (interactive) {
		rl.prompt()
	}

	const history = []

	for await (const line of rl) {
		if (!interactive) {
			console.log("> %s", line)
		}

		const postData = state.input || {}

		{
			const proceed = line !== "."
			if (proceed) postData.vez = 'fordulo'

			if (proceed) {
				if (line.match(/^(|\d+(\s+\d+)*)$/)) {
					const [kon = null, vesz = null, prim = null, szek = null] = line.split(/\s+/)
					if (kon) postData.kon = kon
					if (vesz) postData.vesz = vesz
					if (prim) postData.prim = prim
					if (szek) postData.szek = szek
				} else {
					console.log("Invalid input: %o", line)
					return rl.prompt()
				}
			}

			console.log("Sending: %O", postData)
		}

		await new Promise((resolve, reject) => {
			const postDataString = queryString.stringify(postData)
			const req = http.request("http://atomeromu.anza.hu/reaktor.php", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': Buffer.byteLength(postDataString),
					'Cookie': phpsessid ? `PHPSESSID=${phpsessid}` : "",
				},
			})
			req.end(postDataString)

			req.on('response', res => {
				{
					for (let setcookie of res.headers['set-cookie'] || []) {
						const cookieMatch = /^PHPSESSID=([^;]*)/.exec(setcookie)
						if (cookieMatch) {
							phpsessid = cookieMatch[1]
							console.log("Session updated to %s", phpsessid)
						}
					}
				}

				{
					res.setEncoding('binary') // will convert to utf-8

					let body = ""
					res.on('data', chunk => body += chunk)

					res.on('end', () => {
						require('fs').writeFileSync('./response.html', body)
						body = windows1250.decode(body)

						const data = {}

						{
							let prefix = ""
							const cellRe = /^\s*<TD[^>]*>((?:[^<]|<\/?FONT[^>]*>|<\/?STRONG>)*(?:\w|\d)(?:[^<]|<\/?FONT[^>]*>|<\/?STRONG>)*)<\/TD>(?:<\/TR>)?(?:<\/TABLE>)?$/mg
							for (let cell; cell = cellRe.exec(body);) {
								const string = striptags(cell[1])
									.replace(/&nbsp;/g, "")

								if (string === "") continue

								if (string.match(/^\d+(\.\d+)?$/)) {
									data[prefix] = Number(string)
									// console.log("%s: %s", prefix, data[prefix])
								} else {
									prefix = string.replace(/:$/, '')
								}
							}
						}

						data.input = {}

						{
							const inputRe = /<INPUT[^>]* value="([^"]*)"[^>]* name="?(\w+)(?=[\s>"])[^>]*>/gm
							for (let inputMatch; inputMatch = inputRe.exec(body);) {
								const [, value, name] = inputMatch
								data.input[name] = Number(value)
							}
						}

						if (data.input['kon'] === undefined || !String(data.input['kon']).match(/\d+(\.\d+)?/)) {
							reject(new Error("Form is il-formed"))
						} else {
							console.log("New state: %O", data)

							state = data
							history.push({...data})

							if (interactive) {
								rl.prompt()
							}
							resolve(data)

							fs.writeFileSync('chart.json', JSON.stringify(history, null, 2))

							{
								const output = require('d3node-output')
								const d3nLine = require('d3node-linechart')

								const lines = {
									'blue': "Aktivitás",
									'darkblue': "Hasadóanyag",
									'red': "Hőfok C",
									'yellow': "Hőcserélő C",
									'orange': "Generátor MW",
									'green': "Kontrollrudak",
								}

								const data = Object.values(lines).map(key => history.map(state =>
									({key: state["Eltelt idő"], value: state[key]})))
								data.allKeys = Array.from(new Array(40).keys())

								output('chart', d3nLine({
									data,
									lineColors: Object.keys(lines),
									container: `
<meta http-equiv="refresh" content="1" />
<div id="container">
	<h2>Line Chart</h2>
	<div id="chart"></div>
</div>`,
								}))
							}
						}
					})
				}
			})
		})
	}
}
